# tailwind-challenge
1. เขียน HTML และ CSS โดยใช้ Tailwind CSS ให้ใกล้เคียงที่สุด
file : challenge-css-styling

2. - ทำการสร้าง project โดยใช้ nextjs framework verstion 14 เป็น TS
- install Tailwind CSS
- ทำการสร้าง  Component ให้ใกล้เคียงที่สุด
- ทำการ call api จำก https://dummyjson.com/products

<!-- step install -->
clone project `git clone https://gitlab.com/NatthawatS/tailwind-challenge.git`

cd to project `cd next-typescript-tailwind`

install package node 18 `npm install or pnpm install`

run project `pnpm run dev`
