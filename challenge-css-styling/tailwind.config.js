/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    fontFamily: {
      sans: ["ui-sans-serif", "system-ui"],
      serif: ["ui-serif", "Georgia"],
    },
    extend: {
      colors: {
        "primary-basic": "#E50757",
        "primary-social": "#F26529",
        "primary-marketing": "#08A02A",
      },
    },
  },
  plugins: [],
};
