import https from "https";
import axios, { AxiosError, AxiosResponse } from "axios";

const AxiosAPIHook = () => {
  const client = axios.create({});

  client.defaults.httpsAgent = new https.Agent({
    rejectUnauthorized: false,
  });

  return {
    async get(
      url: string,
      requestHeaders = { "Content-Type": "application/json" }
    ) {
      return client
        .get(url, {
          headers: requestHeaders,
          validateStatus: (status: number) => {
            return status >= 200 && status <= 304;
          },
        })
        .then((response: AxiosResponse) => {
          return response.data;
        })
        .catch((error: AxiosError) => {
          //Logs a string: Error: Request failed with status code 404
          console.error(error);
        });
    },
  };
};

export default AxiosAPIHook();
