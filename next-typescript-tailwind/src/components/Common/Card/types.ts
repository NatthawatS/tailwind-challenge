import { ProductsTypes } from "@/pages/api/types/getProducts";

export type CardProps = ProductsTypes & {
    isFavorite: boolean;
    onClickFavorite: () => void;
  };
  