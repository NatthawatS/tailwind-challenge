/* eslint-disable @next/next/no-img-element */
import { numberFormatUtil } from "@/lib/utils/NumberFormat.util";
import React from "react";
import { StarRating } from "../StarRating";
import { FavoriteIcon } from "../FavoriteIcon";
import { CardProps } from "./types";

const Card = (props: CardProps) => {
  const {
    title,
    description,
    price,
    thumbnail,
    rating,
    discountPercentage,
    isFavorite,
    onClickFavorite,
  } = props;

  return (
    <div className="border border-gray-300 py-8 px-4 gap-4 flex flex-col">
      <img src={thumbnail} alt={title} className="w-full object-cover" />
      <div>
        <h3 className="text-lg font-semibold">{title}</h3>
        <p className="text-sm text-gray-500">{description}</p>
        <StarRating rating={rating} />
        {discountPercentage && discountPercentage > 0 ? (
          <p className="text-lg font-semibold mt-2 text-red-600">
            {numberFormatUtil(price - (price * discountPercentage) / 100)}
            <span className="line-through ml-2 text-gray-400">
              {numberFormatUtil(price)}
            </span>
          </p>
        ) : (
          <p className="text-lg font-semibold mt-2">
            {numberFormatUtil(price)}
          </p>
        )}
      </div>
      <FavoriteIcon isFavorite={isFavorite} onClickFavorite={onClickFavorite} />
    </div>
  );
};

export default Card;
