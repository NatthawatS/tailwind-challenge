import { Fragment, PropsWithChildren } from "react";

const Layout = ({ children }: PropsWithChildren): JSX.Element => {
  return (
    <Fragment>
      <div className="min-h-full w-full bg-white">{children}</div>
    </Fragment>
  );
};

export default Layout;
