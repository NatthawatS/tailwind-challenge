export const StarRating = ({ rating }: { rating: number }) => (
  <div className="flex mt-2">
    {[1, 2, 3, 4, 5].map((item) => (
      <svg
        key={item}
        xmlns="http://www.w3.org/2000/svg"
        className={`h-5 w-5 fill-current ${
          item <= rating ? "text-orange-500" : "text-gray-300"
        }`}
        viewBox="0 0 20 20"
        fill="currentColor"
      >
        <path
          fillRule="evenodd"
          d="M10 1l2.6 5.3 5.9.9-4.3 4.2 1 5.8L10 14.6 5.8 17l1-5.8-4.3-4.2 5.9-.9L10 1z"
          clipRule="evenodd"
        />
      </svg>
    ))}
  </div>
);
