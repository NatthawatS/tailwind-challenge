import Card from "@/components/Common/Card";
import { ProductsTypes } from "@/pages/api/types/getProducts";
import { PropsHome } from "@/types/pages";
import React, { useReducer } from "react";
import {
  addFavoriteProduct,
  removeFavoriteProduct,
} from "./store/reducerSelectFavorite/actions";
import reducer from "./store/reducerSelectFavorite/reducer";
import { useRouter } from "next/navigation";

const HomePage = (props: PropsHome) => {
  const router = useRouter();
  const { data, limit } = props;
  const [state, dispatch] = useReducer(reducer, []);

  const findProductId = (product: ProductsTypes) =>
    state.find((item) => item.id === product.id) !== undefined;

  const handleFavorite = (product: ProductsTypes) => {
    if (findProductId(product)) {
      return dispatch(removeFavoriteProduct(product));
    }
    return dispatch(addFavoriteProduct(product));
  };

  return (
    <div className="flex flex-col pt-24 px-4 gap-12 md:px-8 lg:px-28 lg:max-w-[1440px] lg:my-0 lg:mx-auto">
      {state.length > 0 ? (
        <>
          <h1 className="text-4xl font-bold">Favorite</h1>
          <div className="grid grid-cols-1  gap-4 md:grid-cols-2 lg:grid-cols-4 ">
            {state.map((product) => {
              return (
                <Card
                  isFavorite={findProductId(product)}
                  onClickFavorite={() => handleFavorite(product)}
                  key={product.id}
                  {...product}
                />
              );
            })}
          </div>
        </>
      ) : null}
      <h1 className="text-4xl font-bold">Product</h1>
      <div className="grid grid-cols-1  gap-4 md:grid-cols-2 lg:grid-cols-4 ">
        {data.map((product) => {
          return (
            <Card
              isFavorite={findProductId(product)}
              onClickFavorite={() => handleFavorite(product)}
              key={product.id}
              {...product}
            />
          );
        })}
      </div>
      <div className="flex justify-center my-8">
        <button
          className="bg-orange-500 hover:bg-orange-700 text-white font-bold py-2 px-4 rounded"
          onClick={() => router.push(`/?skip=${0}&limit=${limit + 12}`)}
        >
          Load More
        </button>
      </div>
    </div>
  );
};

export default HomePage;
