import { ProductsTypes } from "@/pages/api/types/getProducts";
import { ActionFavoriteProductEnum, ActionType } from "./types";

export function addFavoriteProduct(product: ProductsTypes): ActionType {
  return { type: ActionFavoriteProductEnum.ADD_FAVORITE, payload: product };
}

export function removeFavoriteProduct(product: ProductsTypes): ActionType {
  return { type: ActionFavoriteProductEnum.REMOVE_FAVORITE, payload: product };
}
