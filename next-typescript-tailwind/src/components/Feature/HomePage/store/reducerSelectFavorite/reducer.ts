import { ActionFavoriteProductEnum, type ActionType, type InitialState } from "./types";

const reducer = (state: InitialState, action: ActionType) => {
  switch (action.type) {
    case ActionFavoriteProductEnum.ADD_FAVORITE:
      return [...state, action.payload];
    case ActionFavoriteProductEnum.REMOVE_FAVORITE:
      return state.filter((product) => product.id !== action.payload.id);
    default:
      return state;
  }
};

export default reducer;
