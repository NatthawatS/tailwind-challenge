import { ProductsTypes } from "@/pages/api/types/getProducts";

export type InitialState = Array<ProductsTypes>;

export enum ActionFavoriteProductEnum {
  ADD_FAVORITE = "ADD_FAVORITE",
  REMOVE_FAVORITE = "REMOVE_FAVORITE",
}

export type ActionSetProduct = {
  type: ActionFavoriteProductEnum.ADD_FAVORITE;
  payload: ProductsTypes;
};

export type ActionProductDelete = {
  type: ActionFavoriteProductEnum.REMOVE_FAVORITE;
  payload: ProductsTypes;
};

export type ActionType = ActionSetProduct | ActionProductDelete;
