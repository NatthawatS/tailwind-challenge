import { api } from "@/lib/constants/endpoint";
import AxiosAPIHook from "../../hooks/UseAxios.hook/index";
import { ResponseProductType } from "./types/getProducts";

export const getProducts = async (props: { limit: number }) => {
  const query = `?limit=${props.limit}`;

  const dataProduct: ResponseProductType = await AxiosAPIHook.get(
    api.products(query)
  );

  const { products: data, total, skip, limit } = dataProduct;

  return { data, total, skip, limit };
};
