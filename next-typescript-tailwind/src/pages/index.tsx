import { Fragment, useEffect } from "react";
import Head from "next/head";
import dynamic from "next/dynamic";
import { getProducts } from "./api";
import { PropsHome, StaticResultHome } from "@/types/pages";
import { GetServerSidePropsContext } from "next";
import { useRouter } from "next/navigation";

const DynamicHomePage = dynamic(() => import("@/components/Feature/HomePage"), {
  loading: () => <p>Loading...</p>,
});

const Home = (props: PropsHome): JSX.Element => {
  const router = useRouter();

  useEffect(() => {
    router.push(`/?skip=${0}&limit=${12}`);
  }, [router]);

  return (
    <Fragment>
      <Head>
        <title>Do Home</title>
        <meta charSet="utf-8" />
        <meta
          name="description"
          content="project โดยใช้ Nextjs framework verstion 14 ที่เป็น Typescript Tailwind CSS"
        />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="robots" content="all" />
        <meta name="google" content="nositelinkssearchbox" />
        <meta name="google" content="notranslate" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <DynamicHomePage {...props} />
    </Fragment>
  );
};

export const getServerSideProps = async (
  context: GetServerSidePropsContext
): Promise<StaticResultHome> => {
  const { query } = context;

  let newLoadMore = {
    skip: 0,
    limit: 12,
  };

  if (query.skip && query.limit) {
    newLoadMore = {
      skip: parseInt(query.skip as string),
      limit: parseInt(query.limit as string),
    };
  }

  const products = await getProducts(newLoadMore);

  return {
    props: {
      ...products,
    },
  };
};

export default Home;
