export const api = {
  products: (query: string) =>
    `https://dummyjson.com/products${query}`,
};
