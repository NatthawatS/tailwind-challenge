import { ProductsTypes } from "@/pages/api/types/getProducts";
import { GetStaticPropsResult, NextPage } from "next";

export type PageHome = NextPage;

export type PropsHome = {
  data: Array<ProductsTypes>;
  total: number;
  skip: number;
  limit: number;
};

export type StaticResultHome = GetStaticPropsResult<PropsHome>;
